require('dotenv').config({ path: '../variables.env'});
const Cliente = require('../../models/Cliente'); 

const handlers = ({ axios }) => ({

    bancos: async(req, res) => {

        try {

            const { data } = await axios.get(process.env.BANCOS);

            // console.log(data.banks);

             let info = data.banks;
            // let info2 = info.map(info => { 
            //      return info.name
            // });

            // console.log("data map", info2);

            // for(let i = 0; info.length; i++){
            //    console.log(info[i]);
            // }
        
           res.status(200).send(data.banks);

            // res.status(200).send(info2);

        } catch(error) {
            console.log(error);
        }

    },

    agregar: async(req, res) => {

        try {
            const { nombre, rut, email, telefono, bancodestino, tipocuenta, numerocuenta } = req.body;

        //    const cliente = await Cliente.findOne({rut});

            // if(cliente.email === email) {
            //   res.json({
            //       status: 400,
            //       msg: 'Ya existe cliente' 
            //   });
            // }

            if( !nombre || !rut || !email || !telefono || !bancodestino || !tipocuenta || !numerocuenta ) {

                res.json({
                    status: 400
                });
            }

            const registro = new Cliente(req.body);

            await registro.save();

            res.json({registro});
        } catch(error) {
            console.log(error);
        }
    },

    busqueda: async(req, res) => {
        try {
            // const { email } = req.body;

            //  let cliente = await Cliente.findOne({email});

             let cliente = await Cliente.find();

            // if(cliente.email ===! email || !email ) {
            //     res.json({
            //         status: 399,
            //         msg: 'cliente no existe' 
            //     });
            // }

            // console.log("cliente", {'nombre': cliente.nombre, 'email': cliente.email, 'bancodestino': cliente.bancodestino, 'tipocuenta': cliente.tipocuenta, 'rut': cliente.rut});

            // res.json({'nombre': cliente.nombre, 'email': cliente.email, 'bancodestino': cliente.bancodestino, 'tipocuenta': cliente.tipocuenta, 'rut': cliente.rut});


            res.json({cliente});

        } catch(error) {
            console.log(error);
        }
    }
});

module.exports = handlers;