const express = require('express');
const router = express.Router();
const { transaciones } = require('../modulos');
const histHandlers = transaciones();


router.get('/', 
   histHandlers.historial
);

module.exports = router;